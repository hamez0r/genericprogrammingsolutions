#include <iostream>

template<typename AttackPolicy, typename DefencePolicy> 
class Player : private AttackPolicy, private DefencePolicy {
	using AttackPolicy::Attack;
	using DefencePolicy::Defend;

public:
	auto Play() -> void {
		this->Attack();
		this->Defend();
	}
};

class MageAttack {
public:
	auto Attack() -> void {
		std::cout << "Using Pyroblast" << std::endl;
	}		
};

class MageDefence {
public:
	auto Defend() -> void {
		std::cout << "Using Frostbolt" << std::endl;
	}
};

class WarriorAttack {
public:
	auto Attack() -> void {
		std::cout << "Mortal Strike" << std::endl;
	}		
};

class WarriorDefence {
public:
	auto Defend() -> void {
		std::cout << "Shield Block" << std::endl;
	}
};

auto main() -> int {
	auto mage  = Player<MageAttack, MageDefence>();
	auto warrior = Player<WarriorAttack, WarriorDefence>();

	mage.Play();
	warrior.Play();


	return 0;
}
